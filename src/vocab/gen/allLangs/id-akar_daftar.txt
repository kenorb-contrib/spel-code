hnit	infinitif
hnak	nominatifkasus
hkak	akusatifkasus
htak	temakasus
hpap	ayah
hmam	mama
hyat	orangtua
hcan	Halo
hwik	bentukpenyerukasus
hsuc	leluhur
htat	eyang
hcac	lapar
hnik	genitifkasus
hcit	katasifat
hwan	dan
hman	saya
hnan	kamu
hwam	kami
htit	sayat
htac	tujuankasus
htik	datifkasus
hyit	realissuasanahati
htim	deontissuasanahati
hcut	bersyaratsuasanahati
hsit	epistemiksuasanahati
hyak	instrumentalkasus
htan	pedalamankonteks
hpan	orangkonteks
htin	klausaindependen
hyan	tergantungayat
hfap	perfektifaspek
hkat	lalutegang
hnin	maskulinjeniskelamin
hmak	komitatifkasus
hnat	menyajikantegang
hpak	benefaktifkasus
swik	essivekasus
htut	ygmenolakpembilang
hsan	permukaankonteks
hpin	adpositionalkasus
hwin	tempatkasus
hkan	ruangkonteks
hnac	carakasus
hyut	interogatifsuasanahati
hnam	danatau
hkit	eksklusif-atau
hsuk	sumberkasus
hpik	ablatifkasus
hcat	kedekatan
hmat	demonstratif
hkuk	klausul-ekor
hlan	tapi
hsut	semua
hyin	universalpembilang
hcin	wanitajeniskelamin
hpac	jamakjumlah
hyik	satu
htic	mereka
hkik	siapa
tcat	masadepantegang
hsin	sosialkonteks
hkac	kapan
hmip	tdksempurnaaspek
klan	sana
hlak	relatifkasus
hlik	terakhirkasus
htam	direktifsuasanahati
hlat	fakultatifpembilang
hkam	apa
hwat	pertanyaankata
htap	katakerjapenghubung
hpat	orang
nyan	pria
hkas	bisa
tcan	lain
hnuc	jumlah
twit	tegaspembilang
hsak	waktukonteks
htuk	sementarakasus
htun	hanya
htum	ygmenegursuasanahati
hyip	retrospektifaspek
hlit	volitivesuasanahati
pcic	harus
tyut	dua
hsat	melihat
hkut	tahu
hcic	turun
hcap	setelah
hpam	pertama
hlac	alienablemilik
hkap	takpernah
hnic	tidakdapatdicabutmilik
tyat	tua
tcit	konsultatifsuasanahati
tcin	hari
hlam	datang
hwak	vokal
hkin	pergi
lwak	perlativekasus
hpip	progresifaspek
lyan	panjang
hsac	mengatakan
kyan	bagaimana
htas	lagi
hsic	sini
hcik	berpikir
hcak	dibawahkonteks
hfan	ditemukan
hsun	tangan
hyac	juga
nwan	muda
tyit	terhadap
tlan	Allah
twak	delativekasus
hmin	tidakada
hyas	sekali
hlap	semelfactiveaspek
kcac	kiri
hyic	gergaji
hcas	rumah
hwac	dunia
tyin	tiga
hnun	neo
hcuk	fokus
pyan	cinta
hsis	selalu
hyam	malam
hyap	raja
hkic	antara
hmas	pikiran
cyit	jantung
pluc	paucaljumlah
kyik	karena
ksak	kausalkasus
tcun	matang
hnim	nama
tsan	tepatkatabenda
klac	negara
hpun	tanya
cyat	terlihatygberdasarataskenyataan
pcac	lebihbaik
hmac	saat
hnas	tidakbisa
hwic	air
twim	Desiderativesuasanahati
kwan	cahaya
tlat	kata
tyan	pintu
hsas	pagi
syat	sampai
twik	terminativekasus
hpit	putih
pcan	uang
hyis	miskin
mwan	kematian
hfam	untukm
nwik	inessivekasus
kcat	bersama
tsac	sering
pyat	teman
psan	setengah
hpim	sangatpentingsuasanahati
hyuc	bulat
hfat	mengirim
hcis	istri
kcan	percaya
hsik	publik
htus	negarakonteks
hpas	putra
clat	anak-anak
tyak	demikian
cwan	berharap
syin	sayang
hkuc	tinggi
tyac	Bacabaca
syit	kota
hlic	sudah
hfit	faktifygberdasarataskenyataan
tcac	gadis
hcun	mendengar
ksan	palingsedikit
hyaf	mungkin
kyat	memang
tcic	tubuh
hwit	kembali
cwat	udara
hfaf	hukum
hput	membantu
lyat	surat
ksas	empat
hpic	optatifsuasanahati
hfuk	api
hcam	jam
twat	perang
kyak	beberapa
tlit	kedua
pcin	dibelakang
hmut	kalah
clan	ribu
hfac	keluarga
twan	jiwa
hwas	pertanyaan
syan	kebenaran
htis	penyusunan
tsat	potensisuasanahati
lwik	allativekasus
nyin	antropikjeniskelamin
hfak	lima
tcak	samasubyekpenanda
htuc	berbedasubyek
hnap	menjawab
hmit	laut
hcuc	takut
pcat	memahami
syas	hal
hcaf	keraguan
twut	dubitativesuasanahati
hkis	hitam
tlak	membawa
hyus	indah
nyit	menyortir
htip	sepuluh
ksis	cerita
hpuk	Book
hlin	elektronik
hkun	rekening
lyas	bunga
lyit	menulis
pwat	tempattidur
hnis	usia
hnut	kontrafaktabersyarat
slas	seni
hwis	vialiskasus
pyak	saudara
lyin	garis
tyas	meja
hyun	sungai
kyac	mata
tfan	mendadak
kcin	dalam
hmuc	tujuan
cyac	darah
hmap	mawar
tlin	dingin
hyif	meneruskan
tyam	terlambat
hnaf	mengira
hkip	hampir
myan	ranjau
tsin	pengetahuan
hkup	salinan
hsam	suami
hlis	enam
cwit	hidup
cyan	perdamaian
myat	ingat
tyic	alam
hfut	jatuh
swas	layanan
hsap	kecuali
pcit	hakcipta
hyim	tentara
kyas	kuda
plic	masadepan
pcak	barat
hpis	membayar
hyuk	merah
hwun	emas
twin	keinginan
plan	bermain
hpus	kantor
syun	pemerintah
kyic	gereja
htaf	kekuatan
hlun	panjangnya
kyin	perusahaan
kyis	kertas
mlan	kemungkinan
hcim	terutama
cyas	beruang
twac	jendela
nyat	keindahan
hcus	terakhir
htup	tugas
htif	jarak
pyas	kaki
hlas	liar
ksin	hijau
hmaf	kesulitan
myac	tersenyum
hsim	tidur
kyit	dikutip
kwik	quotativekasus
fwan	mengunjungi
slan	mengikuti
psit	kehadiran
hmic	manis
hmuf	mulut
hfin	kaya
tcam	musuh
psuk	rusak
kcap	kapal
pwan	inklusiforang
hfic	mempengaruhi
tcuc	muncul
hkim	rahasia
pyin	silahkan
pwit	precativesuasanahati
pyit	sopandaftar
cwak	jahat
myak	Maret
tcas	tempat
hlaf	tinggal
psat	sebagaigantinya
nlan	biru
psac	mustahil
kcuk	sekolah
kcit	menikah
kcak	makanan
syis	duapuluh
tlac	diam
hcip	bakalaspek
kwin	memberi
tsit	terhenti
pyac	bahasa
hsus	minggu
hcup	ruang
cyap	heran
tfin	ygdibawalahir
fyat	pengadilan
hnip	ygbersifatperibahasaaspek
tsak	menghormati
tlas	bernilai
syuc	pohon
plat	dinding
cyak	surga
twip	inkoatifaspek
mlat	perintah
swit	jussivesuasanahati
mwas	bulan
hwaf	sempurna
tyap	pengalaman
ksac	keadaan
syac	segar
hnuk	duke
kwak	tercakup
tyuk	timur
pwac	batu
hkif	mulia
nyac	arah
cyis	kursi
tyuc	menunggu
tsun	desa
tyik	setuju
hnum	serbamembolehkansuasanahati
slak	keberhasilan
tsic	mencoba
slat	bukit
hmik	musik
hkus	menangis
hsip	tujuh
hpaf	pribadi
nwas	berita
tfac	tempatkejadian
fyan	taman
cwas	mulai
hfik	distributifkasus
lyac	belajar
syik	politik
clin	henry
cyin	berbohong
tsas	melemparkan
cyik	kewenangan
tlap	lantai
cyam	musimpanas
tcut	agama
tcap	melarikandiri
hlut	penulis
pyun	coklat
tcuk	rasasakit
swan	musimsemi
hmun	lembut
kyun	sudut
nyak	merekdagang
tsis	keadilan
psas	menghasilkan
cyaf	tertawa
hwut	lupa
hyup	Eropah
hwap	delapan
psak	lewat
kyap	mengerikan
pyic	wajib
hmim	medium
hsaf	nafas
hmis	bijaksana
hsum	posesifpenanda
ksat	nyaman
ksit	kuburan
tsap	pulau
kyam	kamp
tcum	menyentuh
cyun	nasib
psic	pantai
tyun	domain
mwit	komisifsuasanahati
tlut	gelar
myit	musimdingin
kfan	mengadakan
slac	menjalankan
myin	menit
tyip	memimpin
klas	kaca
pfat	naik
cyic	sia-sia
swam	urusan
syam	aman
tsut	dokter
hlip	ygmengakhiriaspek
kcas	kegelapan
nyun	hangat
hpuc	minum
tlam	contoh
hfun	marah
tsik	dengki
tsam	langsung
hmup	topi
klak	anjing
slin	perak
swac	pelayan
kcam	kebanggaan
pyik	melatih
myut	moral
syak	memasukkan
swat	pedang
kyuk	menyerang
clac	kesehatan
tyaf	ayat
swic	sosial
tfit	pengembaliandana
hkaf	resmi
pcas	sekilas
nyam	kejayaan
tfat	duabelas
pcut	bertunangan
pwin	anggur
mwat	rasa
fyac	besi
syap	mendistribusikan
myap	perdagangan
hyum	menggunakan
fwak	hakim
klat	kapten
twap	penghentianaspek
kcic	ingintahu
kwic	asing
hnif	intensifier
myun	pentingnya
tlic	akhirnya
cyuk	leher
syut	kebebasan
pfan	sisi
kyuc	kering
pcap	kesamping
tfak	ygberdasarataskenyataan
tcik	ygberdasarataskenyataankasus
twun	jauh
tyis	kemajuan
nyap	sempit
plit	bangunan
hpif	halaman
pwic	memegang
syic	sedih
tsip	dosa
syim	sakit
tcis	jujur
pcun	bank
ksap	menerima
lyam	aliran
tcaf	hati-hati
kwac	telinga
hpup	roti
kcut	memilih
nwam	hewan
hsup	Zoicjeniskelamin
nwit	eventifsuasanahati
tcim	samaperistiwapenanda
tlik	perkaliankasus
kyus	menyeberang
hmus	tebal
kfat	tantangan
nwat	tumbuh
myic	hujan
hpum	permukaan
swut	suci
clis	Juli
mwun	mode
hfas	mengajukan
cwac	bahu
kwat	kenyamanan
nwac	siapasaja
tcip	diinginkan
kfac	rumput
hmuk	menarik
plin	pembukaan
kwun	Juni
mlik	ikan
klin	modal
pfut	payudara
twam	klaim
fyin	biaya
nlit	milikmu
mlin	juta
pyut	berdoa
hluk	lelah
ksut	membunuh
cyut	bayangan
psin	berat
kcis	kelabu
pyam	berani
mwac	puas
klam	pemberitaan
ksam	nasional
hfip	lemah
klik	lembah
hkum	spekulatifsuasanahati
clas	kuning
cyuc	panas
mlac	sisa
syip	sama
pwak	kotak
tcif	runcing
psip	prinsip
ksik	konsekuensi
tsim	musim
flan	ilmu
pcam	tampaknya
kcun	membosankan
ksuk	paman
mlit	daftar
pcik	berbahaya
kwas	Agustus
hluc	populer
klit	tidakdiketahui
flak	akrab
fyit	akrabdaftar
lwat	pembaca
plun	berjalan
twas	duakali
pcis	imam
fyas	luka
nlat	istana
myuk	menipu
pfak	kebaikan
tcuf	sembilan
cwap	simpati
plac	ledakan
pyap	menempatkan
lyic	berpakaian
hcum	kebisingan
lwit	literatur
tsuk	pangkat
hfup	badai
kcik	saku
nwap	November
tyim	Desember
psaf	ketiadaan
nlac	populasi
kyaf	terima
slis	seri
pyaf	burung
mlak	pemilik
kwut	mantel
hwuc	hubungan
pyis	cepat
twuc	merokok
nwak	Januari
mlic	mesin
tlis	gigi
ksun	pembakaran
clam	malu
klaf	kualitas
clit	cincin
fyak	frasa
fyam	frasapenanda
kwap	persegi
hwip	Minggu
psis	puisi
pwut	ditakdirkan
kyim	membeli
clic	bidang
pyip	April
kfap	Oktober
psut	benda
kcuc	pahit
nlap	budak
hsok	longgar
htet	teks
hbat	terburuk
hrit	kredit
hzit	fitur
hpet	limabelas
krac	kejam
hduc	debu
hrik	praktis
hron	warna
hnus	hidung
hric	ramah
srik	menyediakan
hten	kemarin
tlet	hesternaltegang
slap	gagal
hban	memakai
hdat	buta
fran	terbang
hdan	setan
htec	bertahap
hmon	misi
cran	mengorbankan
hran	hatinurani
hmos	menteri
nrit	energi
hcet	benci
hset	pusat
hrin	rasionaljeniskelamin
hcot	menikmati
hrip	singkat
htep	menyembah
hcif	kulit
hrat	atap
kran	mahkota
hkon	kastil
tran	putusasa
hmif	mil
trin	lingkaran
hgin	penjara
hkop	cangkir
hcen	anggota
swin	sipil
hrac	dimanamana
hris	adat
hbin	menjadi
hmot	daging
hdut	susu
hwen	nabatijeniskelamin
nran	langka
hbac	sepupu
crut	jembatan
hsoc	saham
hsom	garam
nris	tidakbersalah
hbit	menghindari
crin	kekaguman
rwin	ratu
hgut	menghitung
hdas	menari
hnot	hotel
hdin	mengisi
hton	tawanan
hdak	toko
hlet	tangga
hson	Maaf
hgek	homoseks
hras	pasir
hsep	September
hbic	kehancuran
hlen	lisensi
hcon	cukup
hgit	saksi
kfon	dapur
hbak	menjual
tlim	pengobatan
hsot	kecepatan
kyut	komputer
hnen	kekal
htek	perpustakaan
hzan	mengandung
hbis	mendasarkan
hbun	kebingungan
kcus	memasak
hgat	gurun
hwus	ciuman
hbif	es
hbot	bola
hrut	argumen
hdit	doasuasanahati
hdis	berdosa
hjan	hukuman
plis	polisi
hgan	menyembunyikan
hsen	ekspedisi
nrin	bodoh
hyen	awan
frif	Februari
hdik	penyakit
hnek	prakarsakasus
hbam	bibi
hmen	merendahkan
hdip	terlambatsangatpenting
prat	melaporkanygberdasarataskenyataan
hdet	tengahmalam
hjit	jari
hmoc	suatutempat
pran	tekan
kcec	wilayah
hken	belakanganini
hloc	melankolis
hrak	staf
htes	tatapan
rwan	Penghargaan
hdon	menyumbangkan
htot	dahi
hnet	malaikat
hdun	tunggal
croc	kesalahan
hlon	fajar
hyec	domba
hjin	kesabaran
hrec	referensial
mran	jagung
htos	mayoritas
hcos	langkah-langkah
hkoc	kadang
hlif	keuntungan
clak	gula
hjat	kemanusiaan
tram	bingkai
hdic	cakram
hkok	kesepian
srat	artis
crak	pesona
hmet	e-mail
hden	keputusan
gran	gubernur
crac	cabang
hjac	antusiasme
hdim	induktifsuasanahati
hcok	buru-buru
hyet	minyak
pwap	pipa
trat	mencetak
tcot	padatan
hdoc	aduh
hren	ceramahkonteks
tcon	tengahhari
krin	kabin
plam	alarm
hnes	gugup
tcen	pengamatan
hsek	aspek
plik	berlaku
slik	sutra
hfen	sen
hgen	agen
hnon	serikat
crik	neraka
tcet	cacat
mrat	campuraduk
hdam	peringatansuasanahati
hgac	ambisi
htoc	entahbagaimana
hdac	nasihat
nwin	konstitusi
srit	instrumen
hret	sungguh
nlin	online
hzat	pasang
klic	perguruantinggi
hkek	ekor
nric	generasi
hgic	ramai
hgak	kucing
trit	langsungygberdasarataskenyataan
tyot	perjanjian
hnok	isi
crit	ditinggalkan
slen	sebelas
srin	naluri
hpok	suku
hyot	gelisahsuasanahati
cric	berlindung
hruc	kasar
cwin	riding
hzak	menggoyang
dyit	industri
hpec	profesi
nras	perawat
hcec	pengajaran
hbut	sepatu
hmec	samar
twot	aktivitas
crat	Sabtu
hkot	kapasitas
hnec	sinarmatahari
hwuf	janda
cren	gudang
nrat	bintang
hget	tamu
hbik	memberkati
brin	batin
hpot	tersedia
trac	perlawanan
prac	memperoleh
hcoc	berongga
hpon	propinsi
hwim	peradaban
dwan	menggantung
cwok	syok
hdok	penggerak
crun	harta
hzik	benih
hrot	barulalutegang
hket	bangun
hcek	mebel
hrop	tali
mrac	petualangan
byit	pesta
trak	tahunan
hzin	selatan
sric	upacara
clik	pemburuan
trap	liberal
hrim	Kekristenan
hrus	puteri
blat	altar
cyon	mungil
hwep	jaringan
krat	kardinal
tris	ungu
kcot	mengumpulkan
hben	pemilihan
trik	mabuk
kfit	mengangkat
tcec	bertukar
nrik	pastiartikel
grat	kaisar
dran	administrasi
hpen	sebelumnya
cluk	jelek
nrac	takadasalahnya
ryin	latihan
hrun	gulungan
nwic	inci
slit	menembak
gzat	koran
krot	pemotongan
drit	jenggot
hzis	airmata
hdap	delapanbelas
sris	historis
mrut	kotor
nyik	lumpur
bran	pengantin
drat	tergantung
djan	raksasa
glat	kesederhanaan
hpoc	musimgugur
pras	kerudung
hgot	pertemuan
tret	teater
tras	perbandingan
rwit	menyaingi
hfon	membentuk
klot	pelatih
hmum	kelucuan
drac	ceramah
rwas	novel
tset	dendam
hdot	distaldemonstratif
ryut	cemburu
hwon	undangan
byat	dibakar
flac	flash
prot	potret
crek	menyarankan
htok	konflik
htop	kutukan
prit	ygmengutuksuasanahati
hces	takberdaya
klup	klub
hnos	banjir
dwin	pengabaian
prin	horison
mrit	sombongsuasanahati
tcoc	curam
hcom	harmoni
grit	guntur
tlot	masakecil
hgik	lutut
hwes	bawahsadar
hzac	saling
hsec	perangkatlunak
tson	lembaga
ryit	penerimaan
dlin	disiplin
slip	tergelincir
klon	revolusi
hfim	petani
hpom	peron
cyen	mahasiswa
hgec	gua
hroc	tradisi
srac	menyerahkan
ryat	tragedi
kcen	wanitaperawan
hbap	Selamattinggal
krik	kritik
krap	kapas
fwat	rumahsakit
kcet	menyewa
hgas	gas
pfin	terelakkan
dlit	daun
ryan	terjemahan
sran	pemakaman
mrin	tepung
hram	romantis
dlan	dolar
hzas	bersendagurau
hsos	enambelas
klen	koloni
gyin	sayap
hsif	singa
cluc	licik
prip	persiapan
hzut	kabut
dwat	obat
krit	mengkoreksicetakanpercobaan
sras	sarang
hses	empatbelas
brat	keberatan
flat	ACid
hjic	yaitu
slic	senjata
hbuc	pemakai
tron	jeruk
byan	mandi
hrif	primitif
tlip	mendirikan
blan	keranjang
kris	sekretaris
hyon	sementaraitu
tren	hariini
dlat	hariinitegang
hzic	meracuni
tfic	senapan
tlen	bakat
hlim	iklim
dyat	pasangan
kcon	wahyu
dlac	mencuci
krak	pengakuan
hbas	omongkosong
hkec	batubara
hrek	mengembalikan
hkos	kano
hwif	wakil
tric	sementara
hdep	delimitativeaspek
mwak	tumpukan
ksot	tulang
hnoc	Senin
nlas	Selasa
brac	Rabu
frap	federal
pfit	wakil
pcoc	tiang
lyen	linen
swak	wastafel
pcen	presiden
brit	berdarah
hwec	pengepungan
hwet	perut
hlos	diatas
hbon	bermacam-macam
pcon	kuningan
hsem	puncak
lwac	murah
tsos	kontes
kwen	eksklusiforang
trun	pendaratan
trut	patung
hrap	artileri
hgap	menangkap
trot	suhu
grin	lingkungan
hjak	merangkul
hpek	pak
hlus	rusa
hwos	kamartidur
tsuc	semak
pcuc	melepaskan
cris	pewaris
clun	ek
hpes	menyenangkan
nlet	tidaklengkap
hrom	program
pyon	piano
swet	membujuk
dric	gaib
tyen	terkenal
hruk	kekejaman
frat	ditugaskan
hyoc	godaan
hlep	TELICaspek
nrot	internasional
nret	keras
hfif	filsuf
pfic	perwakilan
hmek	peka
hfec	kesejahteraan
myik	penangkapanikan
truc	mencuri
tcop	bibir
rwic	mundur
htom	ekonomi
glan	galeri
pren	paralel
hlek	taktelisaspek
nrak	benua
truk	ketukan
hjim	terlalutinggisuasanahati
rwat	dasi
hcep	sel
dyan	telur
jrat	buatan
nren	inspirasi
ksic	krisis
tsoc	mencari
lwan	makansiang
hpos	pinus
hcuf	mendorong
tcus	menuntut
pset	memperpanjang
hkep	tembaga
swis	pelaut
prim	imperial
trip	cambuk
cras	kemeja
swap	menyapu
syet	sketsa
htof	pencuri
clen	saluran
kwit	kepribadian
klok	kelebihan
hrok	pembaruan
hguc	haus
kcim	mengancam
hpem	telapaktangan
psik	pembicara
frit	ambang
cret	jejak
mrak	tombak
syen	sidang
tcup	basah
hyek	wisatawan
trim	timah
plak	keluhan
hyok	sudut
hgam	sapi
hmok	pabrik
jrac	peta
kloc	kerah
gyit	sengaja
hbip	bir
mlet	beludru
lyap	penyalahgunaan
cwen	kawanan
bwan	keponakan
tses	tigabelas
jrit	membenarkan
nlak	naskah
sron	prajurit
cruc	Jumat
broc	sikat
dlot	Download
krim	krim
pric	nada
prut	juri
hgon	beku
sram	secaraharfiah
pfot	pot
hlot	berencana
djin	koin
hlok	lompatan
hpep	buahapel
hwot	sinting
dlak	membahas
brik	berlangganan
nlis	menuliskan
hbet	bertaruh
pris	virus
hjik	keramahan
blit	kekanak-kanakan
hjap	jubah
tfik	mencerminkan
hdaf	mencapai
hpop	opera
kcaf	bersembunyi
kcoc	siku
hrum	perbaikan
cyim	mencurigakan
dyic	pendidikan
hgoc	lubang
drak	kekacauan
plas	tepuktangan
byin	biner
hzap	biara
nlot	loyalitas
mrik	mekanis
hsop	berhasil
crap	perangkap
kyek	kue
nrut	menghibur
syes	tuan
hjet	tujuhbelas
hjen	wanita
hmom	madu
hdec	sayangnya
lwen	legenda
slot	kompor
frac	pemberontak
gwan	membengkak
mruk	memperbaiki
swot	rawa
zlan	mendapatkan
hnem	animasi
hdes	pembalut
prak	berkhotbah
frin	pemandangan
hfet	menentang
cros	paduansuara
ryon	rasional
tcos	tanaman
tsen	pengolahan
hwek	serigala
nlik	penolakan
klap	karpet
dras	memandangrendah
prop	subur
slim	peluit
hgis	keju
krut	gigitan
srot	pengawal
bjan	mengembara
nwen	angkatanlaut
myas	topeng
kwaf	mengembangkan
twis	taksi
pcip	pensil
srap	sayuran
sren	pameran
tlun	celana
hrup	korupsi
nrip	kabinet
srup	sup
jran	ahlibedah
bjat	tunjangan
gyat	mengintai
hlem	sembrono
srak	abstrak
kson	aksen
cwis	Kamis
sret	sungguh-sungguh
hraf	lalulintas
hres	arsitektur
hsuf	pembuangan
rwak	retak
twon	ornamen
hfus	pemulihan
hkom	kode
djat	pemburu
nlut	laporanberkala
hsef	ular
mris	demokrasi
bric	baterai
mlam	lilin
zran	berbagi
cron	sensasi
hzen	berenang
traf	mengupas
cwaf	amplop
srip	rokok
mrot	motor
swon	sukarelawan
ryak	reaksi
pfun	sebagian
blac	bagasi
mret	kementerian
hjon	seksual
kcok	kapak
dram	berlian
kren	bergantian
tcek	gema
hdif	identitas
tfas	dutabesar
hfot	terdiri
lwas	malas
hmep	momentaneaspek
kfin	encoding
mwin	imajiner
hrem	sinar
pram	pendakian
byac	gudangdibawahtanah
mren	kenangan
swen	senat
kras	petimati
hgun	menelan
nlok	memuaskandiri
ryic	pertanian
gwin	ayunan
srut	padangrumput
hkes	kompleks
hfis	pengundurandiri
frak	berkali-kali
hnom	konspirasi
hcop	pub
hlec	lada
mlot	melodi
zrat	calon
proc	pelindung
flik	rubah
cyet	menyesali
hgim	cerobong
slet	pagar
tcok	ketepatan
bjit	bekasmendiang
brot	tunas
bwat	selimut
hdus	babi
gwat	gading
kret	ekstrak
nwet	pengamat
jwan	kesempatan
nwis	kuku
mwic	lebah
bzat	aktor
gyan	keringat
zyit	eksekutif
blic	ilusi
pcet	ketidaktentuan
tsot	melipat
bwit	batas
tres	menekankan
hkof	konsumsi
nyen	mengerang
hles	menyerupai
cyek	memecat
hjis	jarum
gjan	gejala
prec	bertingkah
drot	dikontrol
kcek	melengkung
twic	ayam
brak	barel
zwat	biasa
clap	biasaaspek
tsec	ahli
bwin	mengesankan
cwet	dibatasi
hbek	tukangroti
zlin	zaitun
mron	mobilvan
drin	mencurahkan
psap	membuka
gjin	ulangtahun
clut	palu
cwun	harum
twaf	sepele
pcot	menimang
krok	tengkorak
dlik	badanlegislatif
cwic	untung
srun	mengerut
crec	menghancurkan
jlan	batas
kcip	juara
blin	milyar
ryik	kimia
mlas	nadi
flit	pilot
hzet	simpul
cwik	megah
twet	rok
mwik	mingguan
tfis	teknis
tlon	kendaraan
cram	menyanjung
grac	memperkuat
kraf	gagak
trup	atribut
hduk	menghubungkan
cruk	harimau
nrap	pengurusrumah
crot	jalanraya
mlip	milisi
hfoc	foto
nres	integritas
plos	plus
tfam	jelas
nlic	sembilanbelas
hwok	berkualitas
nlen	karangan
fyic	efisiensi
hwuk	unik
hgok	organik
flic	teologi
flot	mengapung
hboc	mewajibkan
hwoc	penyihir
syot	studio
cwip	sampanye
hbes	spanduk
lwic	perceraian
blot	baut
cwot	tahanan
cwam	pembuluhdarah
prik	pasif
hfum	busa
grik	kerikil
zrin	celemek
gren	planit
hfuc	kebangkitan
claf	rak
flas	penggemar
hjot	tropis
hbim	sewenang-wenang
krek	airraksa
hbec	penuhkebajikan
tsaf	bunuhdiri
pwas	memetik
plot	reporter
kfak	mengeluarkan
blak	balkon
hdos	noda
kwis	klasik
kcos	menuduh
psen	pensiun
kres	katakerja
hzon	monyet
srim	tandatangan
glin	keengganan
mlut	terkenaljahat
myot	impor
fwac	pendiri
troc	obor
hgaf	tatabahasa
hyop	kapalpesiar
ksip	punah
sres	tamasya
hnup	kubah
hzun	keraskepala
mric	otot
gjat	besarnya
hros	menggosok
zrac	bacul
tros	rotipanggang
myus	mouse
dwac	dialog
byak	kambing
htef	menyesuaikan
hfek	berkembang
dzan	memaksakan
nyon	anonim
zrit	resmidaftar
pcos	olahraga
hjec	mencair
dlas	hapus
mras	payung
mram	mineral
cwut	gencatansenjata
hbok	bebek
swun	sabun
dwak	boneka
plif	vila
slon	jahit
tfut	mengucapkanselamat
djac	mendamaikan
hjas	menekan
kric	kelinci
kwot	konduktor
gzan	menjamin
clot	pemain
ksek	serangga
psam	sikap
mwut	burunghantu
htem	tetap
clon	biola
dzin	penduduk
rwim	masing-masing
hyof	bermanfaat
dron	naga
dwit	pendengaranygberdasarataskenyataan
sroc	mengekspor
hzot	penghentian
djik	masihtertinggal
kfik	tutup
ryun	peraturan
hmes	walikota
rwac	perangkatkeras
sruk	sirkus
sros	minoritas
bzan	kerusuhan
kset	orkestra
bjon	dagingbabiasap
hjut	takterbatas
tfen	membela
zwac	kerugian
dyin	pertimbangan
myis	pemusik
mlis	mental
pret	tidakmemadai
tsek	arsitek
hlom	jeruknipis
ksos	batuk
crok	sakitkepala
flin	kesuburan
mrun	tukangkayu
swec	acak
syuk	sendok
blun	balon
tcep	kapur
fric	minuman
rwik	vertikal
rwen	mencegah
flet	inferensialygberdasarataskenyataan
pfac	penerbit
srek	cuka
tfun	terowongan
pwet	membingungkan
gyik	matematika
fwot	luntur
lwin	seimbang
jwin	hati
bwik	sublativekasus
hdup	menukik
hnop	monopoli
bwic	kapalselam
ryen	tanpamemperhatikan
hcem	gempabumi
hbos	laboratorium
nrek	pekerja
ryim	irama
nyic	menafsirkan
dwas	kedutaan
rwok	memprovokasi
kram	ketidakmampuan
ksim	deklaratifsuasanahati
clok	cokelat
pcec	istimewa
nyis	pertapa
tyoc	kebetulan
grak	menggaruk
zlat	sarungtangan
hjuk	berlutut
trif	geografi
djit	sistematis
pson	takterkalahkan
mwis	investasi
cwec	lisan
hdek	sekuler
blik	elang
sloc	psikologi
brap	membatalkan
jlat	bajaklaut
dros	dosis
slam	ikansalmon
pwun	puding
gwik	magnetik
pcop	pompa
myen	demikianpula
ksen	oksigen
mrap	mandor
tfot	sertifikat
ksus	gugus
trok	retorik
tloc	anaktangga
zyan	dayung
nlun	masuk
hlop	teleskop
jlin	ejaan
lyut	berawan
hsof	berselancar
fyik	wiski
klet	relatif
hwof	willow
hlum	unta
myet	amatir
klis	isolasi
hbaf	biografi
gwac	keledai
hwum	isu
pwik	perspektif
pron	Pro
myes	bulanan
mwen	lamunan
kfic	liburan
klek	klik
cwon	lonjong
lyak	elastis
kwon	kamus
hjam	ham
rwon	pelangi
crip	denganini
zwan	terjadi
srok	tajukrencana
craf	kopral
tlok	tangki
brip	kupu-kupu
tfap	grafik
jwat	kembar
pros	operasi
psun	pertanda
clet	keadaanygmenyedihkan
hbus	bambu
dyas	pedagang
klun	akumulasi
jlit	sepakat
hdom	mimpiburuk
kron	karbon
hzam	selai
cyot	pendengar
hmem	museum
lwam	alfabet
fras	menyebarkan
psoc	meninju
hgum	gusi
brek	rem
glit	stabilitas
frun	membusuk
gris	agresif
mroc	gesekan
bzit	labah-labah
plut	kentang
glac	keserakahan
gzac	berkembangbiak
prun	berputar
jlet	kalender
nyim	pencalonan
tlos	triliun
slut	salad
fwit	setujusuasanahati
pyen	aplikasi
nlon	nominal
dren	mendemonstrasikan
drun	ketidakpuasan
cyok	mengangkatbahu
hfop	sepakbola
ryac	persekutuan
gzin	pelopor
hgip	kubis
tyet	atletis
hdof	idiophone
hwup	takternilai
lwek	tuas
syec	senator
jyak	menampung
mlim	semut
fwic	efektif
gzit	hamil
blas	kepercayaan
jrin	setiaphari
gwak	pergelangankaki
drik	dgnaneh
prif	variabel
jwit	perbedaan
krip	diskriminasi
ryot	terpesona
nwut	trotoar
kcif	aktif
jrak	katak
twek	terlalubanyakkasus
psot	penebusandosa
pfon	sepon
sluk	tupai
nlim	irrealissuasanahati
ksaf	kelangkaan
glak	gunungberapi
plen	keseimbangan
kfaf	syal
kyon	mengeksploitasi
jlik	geologis
psop	paspor
kfis	kekosongan
fyon	afinitas
nram	asusila
hbuk	biskuit
gras	kesurupan
hyos	gelisah
flim	film
plet	tablet
rwam	atom
blis	vanila
zlit	bantu
fyut	rewel
crim	piramida
hjif	melanggarhukum
crif	perasaanmalu
lwap	kecapi
tyon	astronomi
mrim	labirin
drut	deskriptif
nrim	beasiswa
kruc	penyu
kwam	penasihat
jrik	jangkrik
dlut	menunjukkan
pyot	merpati
dyon	adaptasi
prap	operator
bzin	lencana
tyek	segitiga
trec	bendahara
nron	hidrogen
pyet	pencetak
tsok	tersedak
zyin	kronis
slek	sepeda
jyin	teknik
ryas	transmisi
zrak	menyegarkan
nyas	penari
gric	migrasi
zlak	melanggar
tces	tenis
hjun	berangin
twen	menenun
kyet	pengumpul
gwit	tulangbelakang
kyot	senar
fwin	frekuensi
tfuc	mesintenun
dlet	direktori
pfik	pemikir
bjet	anggaranbelanja
trek	strategi
kros	kuarsa
gjit	gunting
gwic	beralih
frik	teoretis
tluc	handuk
pruc	patroli
glic	gletser
krun	kelengkapan
bjin	ramalan
tlaf	ikut
tfet	pabrikan
bras	rusuk
dwik	terdakwa
clec	knalpot
cres	catur
tcef	ceri
tcom	tersandung
nyot	penterjemah
tyok	truk
cyip	melebihi
grot	moster
bris	nirkabel
nlam	lelang
slif	saringan
hgos	karat
swoc	goyah
jyat	bangkrut
slum	Islam
hjes	sensus
dzat	berjinjit
slun	menangguhkan
nwot	kontinuitas
gyic	gitar
pfas	prasangka
zyik	seng
blen	berkilau
hyem	elm
hnep	berebut
nyut	rajutan
ryam	katagantipenanda
clim	membumbui
jret	ketajaman
tfon	menggoda
jwac	Jawa
hrep	resep
dyac	hak
myip	meter
rwis	rasial
hkem	komet
nwon	inovasi
zwin	melarang
dyis	pertengahan
mrec	burunggereja
fyot	catatankaki
mlon	pengukuran
pfis	ratifikasi
rwot	orbit
hfok	hemat
glof	golf
zron	nol
hzip	zip
hwom	tunawisma
bjic	mengunyah
pcim	memenuhi
hjip	keping
bwac	menguap
mlos	tahilalat
mrip	barangtembikar
bron	embrio
ksoc	deduksi
cyum	ragi
jyan	jargon
plim	prem
mwet	makam
ryet	lulus
glik	irigasi
plap	kapitalis
tsif	elastisitas
gyak	hiu
djon	kegagalan
nwun	tepatpdwaktunya
cles	realisme
dris	makin
hkef	kafe
blus	ambulans
blap	dompet
bjak	gembiraluarbiasa
klim	meringis
gyac	ilmuwan
jyit	ketenangan
dzit	realistis
bwak	luarruangan
dyot	radio
bwon	kacang
fris	informalyang
klut	berspekulasi
tfek	steak
nrok	nitrogen
zyat	pisaucukur
bram	bergumam
ryis	pengantar
nlip	menganalisa
nros	kontras
hcof	menanggalkanpakaian
tfif	insentif
kroc	alur
hdum	agamaYahudi
pses	penerima
plok	melengkapi
lyet	lensa
pwek	pegativekasus
kfas	kanker
pwen	menggadaikan
nrun	peringatan
clek	kebocoran
pcif	pemulihannamabaik
glas	kompleksitas
zwit	latah
kces	kecukupan
crem	eceran
grap	mengambil
kwip	campurtangan
dyut	diskon
crus	hirarki
gyet	giok
gram	menggeram
kyip	kadal
nyet	bantalan
crop	airterjun
glon	galon
mrok	nyamuk
fluc	fasih
dwic	deviasi
slec	penjelajah
srec	berakhir
blut	bergulat
myok	mosaik
tlek	steker
nruk	berjangkit
nwoc	untukselanjutnya
bjut	berjongkok
hges	kehamilan
twoc	tokobahanmakanan
kyos	Komunis
mrek	menakjubkan
clip	penyesalan
pwok	prosecutivekasus
dwot	diktator
zyac	keasyikan
jlic	satelit
slop	sandal
ryip	sebanding
psim	kira-kira
byet	penganggur
cyop	hobi
rwun	kriterium
blop	bohlam
syok	pendukung
klos	klorin
lwot	khlorida
byic	bakteri
lyik	lengket
blok	biologis
jlac	berpura-pura
bret	byte
krif	keluarnya
blet	balet
nruc	airseni
tsus	bawangputih
dyak	kelasmenengah
tcem	penjepit
cyoc	menantang
trep	stroberi
dlun	polusi
hlup	album
hbep	baseball
praf	pembajakan
pwis	partisip
grek	garasi
kruk	kruk
hfof	fosfor
rwut	struktural
myam	ruangolahraga
krec	derek
jras	kontraktor
zrik	memberitahu
blam	ballroom
klip	tukang
hwef	belumdibayar
zlac	pisang
gret	generator
lwuk	tulangrusuk
nyek	nikel
blos	bis
sles	sensor
tlif	ubin
kses	sosis
mwot	tomat
lyot	mobil
tsom	kalium
brut	maya
djap	aljabar
hjok	umum
kfen	konsep
pcok	optik
bzut	bergerak
syek	pengangguran
mruc	ulat
bwas	Browser
klif	kalsium
rwet	terpencillalutegang
srif	bersejarahtegang
hwem	spektrum
syom	sodium
tyos	wali
jyic	sandera
hguk	kundur
dwun	kerut
swip	paku
tlec	kutu
blaf	memperkosa
tsem	ateisme
mloc	Monitor
gyot	bersyarat
trop	menyelidiki
rwap	triwulanan
hjep	jeniskelamin
pyim	platinum
dlon	rangkapjumlah
pcep	paru-paru
twuk	penggosokan
tyec	menandai
syup	super
trom	troli
nyip	lokal
glis	bensin
gjik	ginjal
hjos	tanpaperawatan
hjuc	menjarah
swek	terlalubanyakbekerja
psek	arsenikum
twok	statistik
tsep	belalang
slos	sosialis
nwim	indrawiygberdasarataskenyataansuasanahati
tlop	taplakmeja
trus	kontroversial
pwot	rantaianjing
djot	endapan
nloc	penghinaan
mwon	jerukkeprok
hjek	joki
crom	bekerjasama
myon	pneumonia
twec	menempatkan
ryek	roket
dlic	dividen
kwet	sepatuluncur
bwen	obsesi
plon	peruntukan
syon	terengah-engah
djis	digital
byut	menggerutu
hyep	kelopakmata
dwon	diagonal
byis	bawahsadar
brec	alis
cyec	bersin
hwop	watt
clus	riang
kfun	pemilikan
nwum	Hinduisme
fyim	pengungsi
cwos	shuttle
tfoc	tusukan
pwon	protein
sraf	puasdiri
hbom	muntahan
gyen	gen
drim	mahasiswatingkatpertama
trem	perlengkapan
tsup	pecah
slok	sosiologi
kwoc	labu
bwam	pemilih
mlok	mobilitas
kyen	daerah
hmop	gerakanmenanjak
rwek	retrospektif
kfup	kubus
grok	wortel
drap	memikat
lwis	molekuler
ksuc	akuntan
tsop	bau
kyok	ketimun
nroc	sukaria
jlot	pelarut
psof	fosfat
hgif	mengasyikkan
kfim	bersifatalkali
psos	olahragasenam
pfen	verifikasi
kcom	ekonom
krom	koma
tluk	bergeliang
hyom	yodium
nwuc	gerakanlereng
gjac	keguguran
pcom	polong
dzic	perancang
grun	membual
dzot	donor
hzek	manggan
kfot	Keyboard
jlas	tidakbisadiandalkan
lyim	elite
jyen	yen
pcaf	kurung
rwip	ragbi
blip	Alkitab
kyoc	halmenyebabkan
lyon	maag
flis	infleksi
nrif	menggigit
kfam	kuantum
swes	switer
lwon	evolusioner
drec	pembedahan
gjap	spa
lyec	linear
tyom	antimon
pwuk	superessivekasus
flaf	alfa
myim	imun
pcek	penculikan
bjac	tempatpembuatanbir
bzac	membatasi
bzak	kauskaki
twes	televisi
dwut	dokumenter
ksom	kosmos
lyis	kehutanan
lwim	aluminium
ksok	horoskop
sruc	mempercepat
dlif	ikanlumba-lumba
byik	pembunuh
tfec	kertassurat
hjaf	binatangmenyusui
ryok	protokol
dwis	dioksida
jlon	ususbesar
brus	birokrasi
gjak	galaksi
lyip	klinis
ryem	radium
href	jernih
prok	kulkas
slep	satir
dret	perulangan
kluk	kepurbakalaan
dlis	keistimewaan
kfip	tidakdapatditerima
mlec	pijat
pfif	spesifikasi
nrec	indrawi
pfet	leaflet
hzoc	mendasari
dlam	idealnya
nlec	antropologi
kluc	lezat
dyet	sedan
tyif	transitif
krus	gemetarketakutan
nluc	bungamatahari
lyun	ion
zrot	ekstradisi
ksif	memalsukan
psif	emisi
crup	tumpangtindih
fret	penemu
grut	membalikkan
ksec	temansekelas
jric	rencanaperjalanan
mrop	microwave
srus	mengusir
gyas	penatarambut
floc	ceritarakyat
plek	kapiler
nwop	pantat
nrom	pengawas
twos	terorisme
lwok	slogan
dwet	lembur
slem	menghaluskan
grip	panggangan
gwas	Wow
kwok	kobalt
gwon	haver
hdop	gerakanhulu
glam	dubur
bwot	termasukkeluargasapi
bren	usus
byot	video
bjik	memboikot
kwim	ulangan
cwek	hoki
grif	gerilya
hgem	magnesium
hyes	kesadaran
fram	farmasi
nwos	tidakberambisi
ryap	mengaturkembali
cwim	keandalan
klep	kelp
psec	membayarkembali
djic	diseluruhdunia
kfut	jammalam
crep	hamburger
frim	dalil
gjas	menyalakan
hdef	debet
ryum	uranium
pluk	menilai
krop	koordinat
nraf	pembayarpajak
tyes	terpadu
plip	berpendapat
hgom	gamma
hgus	koefisien
cwuc	pemanas
zret	mengurangi
nles	analis
trof	intransitif
hmof	mall
hzim	bismut
dzac	kantin
psok	kartupos
blon	konvergensi
djet	ngelantur
ksum	pemegangsaham
mrup	mikroba
clif	spiral
cloc	morfologi
tfes	pemborong
gjis	jeans
jren	penjajaran
tlep	Database
kcup	kupon
glot	tegangan
glut	Kalkulator
srep	duniamaya
nwes	investor
ksef	sinarX
bzis	Visa
frut	infrastruktur
nyuk	kunang-kunang
dyik	detektor
plec	sambatan
brom	brom
plop	proteksi
zlik	silikon
flut	volt
ksop	kosmetik
gjun	dandan
kfok	cegukan
hpof	jagungmeletus
hzuk	kebunbinatang
nyoc	konotasi
jwak	yoga
brim	barium
kles	memenggalkepala
mwam	ya
fwik	kembangapi
djas	jazz
dyim	audio
tsum	tungsten
drip	menggiringbola
lyaf	alimentasi
prom	programmer
nrup	memasukkan
hfes	refleksif
rwaf	krisan
dlen	dokumentasi
byas	vaksin
fron	heroin
dluk	memutuskan
dyun	mendiagnosa
flap	map
kfet	katoda
dlim	ideologi
prek	parkir
syoc	foto
slus	aksesibilitas
nlos	antiklimaks
mlen	selenium
pres	lapisanluar
hlof	sulfur
nlek	arushubunganpenanda
gwot	bandara
mwip	penipu
klop	kecoak
kwom	khrom
hl6t	liter
nwec	namun
glip	berhubungdgnputaran
nrop	daftargaji
crum	hormon
dlep	pembangun
frot	melubangi
bwoc	sabotase
fwis	feminis
djok	mengkonjugasikan
draf	memodernisasi
trum	ekstremis
graf	algoritma
lwes	laser
mlem	milimeter
flen	melenturkan
zlot	anoda
swok	merapat
klec	anjinglelah
pwim	paruhwaktu
htuf	ikantongkol
fyis	kilangminyak
lwet	kepekaan
mrem	mainframe
jwic	peremajaan
droc	demonstran
clem	helium
dzik	dwibahasa
psem	plasma
hjom	kromosom
rwuc	urut
tyus	bilanganbulat
klom	kilometer
hfos	fax
nwek	genetika
jrap	obeng
zwas	hidanganpembuka
bwok	brokoli
dwap	adaptor
syem	Spam
blek	vektor
tlus	berhubungdgnsusunan
pyif	ygtakditentukan
cwoc	baik
mrep	amper
hbem	vitamin
hxan	penjagapintu
hxut	menyubsidi
hk6m	kadmium
hxin	keserentakan
txin	titanium
kxin	kilo
klum	bersanggama
xlis	jampasir
hqak	berhubungdgnsintaksis
hxat	kreativitas
hxik	ebook
hxas	tempatkerja
bxon	boraks
dxin	induktansi
hxit	helikopter
tlum	telurium
txit	batasakhir
kxat	mengoksidasi
hvik	meluruskan
nluk	analog
nyem	sentimeter
hm6t	molibdenum
hl6m	lithium
ht6t	multimedia
xlit	kalori
ryop	robot
pxas	apoteker
hxak	pekerjaanrumah
lwif	lokatif
grim	logaritma
hqat	mantis
qlit	ekologi
xlik	orangygberpengalamandlmhal-halduniawi
mlep	Template
hqit	radioaktivitas
kxim	kilogram
hxen	penyewa
hxot	makanterlalubanyak
kyif	copier
fwet	freeware
txet	bagian
hs6n	sekuensial
hxet	mendengarkanrahasiaorang
xrat	potongrambut
hqan	menyita
hr6t	iridium
bxat	Bandwidth
dxet	Desktop
hxis	eksponensial
hxok	kantorpusat
hxek	salahmenaruhkan
srem	cerium
hp6t	ygdipisahkan
pxik	ygdipisahkankasus
kxet	kaset
pruk	supermarket
hvit	tekad
xlin	Pengunjung
frip	enkripsi
hvep	membayarlebih
hxon	menyamakan
kxit	memaksimalkan
hqin	seminar
tr6n	stereo
ht6n	pengikat
hvat	mendatang
sl6s	Celsius
hn6n	neon
ts6t	nyatawaktu
hvin	mempersingkat
pxat	kioskoran
myoc	modul
ps6s	pasta
bwek	taskantor
dlok	mewah
fraf	firewall
hr6k	artefak
txat	waktumakansiang
hqon	oregano
pxun	plutonium
ks6n	workstation
slef	eskalator
xrit	perenangpenyelamat
hvut	membuatsesak
xrim	yttrium
hx6t	ygbertambah
hqum	namapengguna
hl6n	penjual
gron	argon
pxan	promosional
hvac	pencucipiring
xlan	globalisasi
txon	panjanggelombang
xrik	mendaurulang
hxap	ransel
hxac	mengulangkembali
xyat	hepatitis
cwep	cekgaji
hxos	torus
txif	jenishuruf
qlat	makelarbarangtakbergerak
txot	pertengahanmusimGugur
hxam	miligram
pxis	pixel
pwif	pratinjau
txam	tantalum
myek	termodinamika
hbop	membobol
hqot	zatterlarut
xlic	kursiroda
gxit	genosida
hvan	pemadamkebakaran
glim	gallium
pfop	popnaik
hzum	zirkonium
xlon	hotline
qlin	nonlinear
kxam	skandium
blim	berili
kxik	mengkonfigurasi
nr6n	neuron
hpef	predikatif
hruf	router
kxan	faktorial
hxic	garispedoman
xlip	bolavoli
dyek	men-debug
hxop	jamtangan
qrot	neutron
nwip	niobium
hs6t	kamarkecil
grem	germanium
tles	metalloid
hp6k	prolativekasus
qyin	keanekaragamanhayati
txak	antibiotika
mlun	imobilisasi
txan	aktinium
xran	astronaut
dres	mengurangitekananudara
tr6t	extraterrestrial
hk6t	oktet
hxus	Bukasumber
xlat	gila
mraf	maraton
ts6n	pertengahanmusimsemi
ht6s	autisme
nlif	mengalamiionisasi
txut	orangterkemuka
hm6n	emitting
kxis	klipseni
xrin	menjalani
xren	mengarahkan
bxak	bookmark
pfos	profrasa
hvis	kotoran
hd6n	penyebut
brum	ruangrapat
hqis	penganutaliransovinisme
hk6n	menyusun
xyit	bergeserkunci
xyin	satudetik
hxun	hidupdari
hvas	peran
glok	blog
hvak	liat-liut
pxit	akupalamat
hvet	disamping
xram	safflower
hc6n	kenyataan
qran	Qur'an
zyen	xenon
hqas	bersumpah-in
bzas	bosan
hj6n	nonmanusia
hnof	datar
txas	pembuat
hbum	agamaBudha
ts6s	pertengkaran
pxin	bahan
hr6n	jatah
hxim	pertengahanmusimpanas
dxit	pertengahanmusimdingin
hxaf	menggali
nlus	tanamanrumah
txap	statusbar
vrit	penulishantu
kxak	bermain-main
bxin	tubuhmanusia
hqik	jashujan
hxes	hertz
hxif	menghipnotis
xras	perjam
pl6k	paralegal
dxat	keadaanterburu
hxip	antibodi
vrat	celanapendek
hqek	membentukkembali
qrit	polaritas
pxot	kantorPos
hxom	rongsokansurat
hqic	resensi
lwun	jinsbiru
hp6n	pematangan
dxan	melampirkan
xrak	aktorperan
dxak	kecelakaan
hven	kontainer
mwap	komik
qlan	butawarna
xros	membukabaju
srom	ruangpamer
hc6t	mengobrol
qrat	terbalik
hn6t	inhibisi
hqet	situsweb
fres	menggalangdana
froc	penempaan
xlak	hyperlink
vlat	keramahanmulut
dvan	mendarat
gxin	menaklukkan
ryes	resepsionis
kxot	konektor
xyan	pembatasan
vlan	pemasangiklan
ryom	thoriumatom
hluf	usiatua
hquk	menyuntikkan
lyum	thuliumatom
tyem	TBatom
hp6m	praseodymium
txic	rumahhalaman
mros	pengendaramobil
dyam	rhodiumatom
pc6n	percobaanjumlah
nyos	inbox
pyek	proyektor
kxon	kripton
hxuc	menyelamatkan
qrik	centrickasus
xwin	kurangpengalaman
kxic	pemadam
hvot	flowchart
dxot	yogurt
hqut	mencatatdiluar
pxen	departemen
drok	tujuanpelatuk
flon	telepongenggam
txos	tengahsuara
hqok	miringkasus
xwik	sabarkasus
hx6n	prahariini
txik	jaritelunjuk
hqen	pengirim
mlap	malware
hvun	pembujukan
hvon	aus-out
vran	surat-carrier
pfes	profesor
pwip	penyair
hqam	bekerja
gxat	legalitas
ks6t	kausatif
vrin	perekrut
kxip	klipnaik
sr6n	ygtdkmenyetujui
vlit	evaluatif
tyup	kotasatelit
mrom	morfem
xron	headphone
gxak	mahal
hrof	trojankuda
xlas	lumpuh
dzet	menghiasilagi
txac	menyerahkandiri
xrot	terpakai
txis	dotmatriks
syif	siunit
xrac	heran
brif	biner-nilai
dlos	modalkasus
txun	atribut
dvat	pemberang
psom	tanganpertama
gjek	lain-jika
nlem	Unit-of-massa
qret	eceranmenyimpan
txip	triple-titik
dlek	sendiri-sendiri
lwut	aliran-saat
dxis	dramatis
hs6m	cesium
vlak	railcar
hn6m	Nerium
kxap	kardus
bxan	berangkat
myem	bulanMei
gxik	hak-haksipil
hxoc	AngkatanUdara
xrok	temansekerja
kr6t	screenshot
txok	secaraotomatis
hxep	skateboard
srop	smartphone
pxak	topimengunci
hk6k	musikDaerah
hl6k	euler's-nomor
pyom	poloniumatom
xyam	taliumatom
hvic	invasi
lyok	lokatifkasus
xlot	flat
txen	uraianbaru
kxac	seranganbalik
qrak	orangygmencobamemecahkanpemogokan
hbup	kolamrenang
txuk	aruspendek
tsef	meruncing
bxit	jaringantuanrumah
ksem	Bacabacahanya
byim	rubidiumatom
hvam	vanadiumatom
lw6n	lanthanum
frum	ftoratom
nyom	neodymium
mlom	multaljumlah
kxas	personalisasi
xret	terabyte
hs6k	pencarianmesin
frus	korosi
gxan	galaksijeniskelamin
vlas	vokalisasi
glet	pedalgas
hvim	runtime
bxip	bitmap
qrin	bintangjeniskelamin
bvak	abessivekasus
pxon	tidaktersedia
dvik	adessivekasus
dvek	agentifkasus
hlef	agenpelatuk
kwek	untukkasus
kxen	umumjeniskelamin
ht6m	kutipanuntukm
tsuf	transangka
hf6n	denda
tc6t	langsungobyek
gvik	ergatifkasus
gxon	fragmentasi
ksep	reseptor
vyin	bioskop
pyos	pasifsuara
hnef	terbataskatakerja
xwit	utamaobyek
hqim	ygbersifatperibahasasuasanahati
jlak	generalisasi
vrik	terakhirpartikel
qyan	imajinerunit
kxaf	kecepatancahaya
xyic	phishing
qyit	titikkritis
hvek	evitativekasus
bvik	subessivekasus
hzes	kerasukankasus
prus	viruskomputer
ples	prosesor
pxif	polisipetugas
dvak	mendapatkankursikasus
brun	touring
tsi_k	antessivekasus
tyum	rutheniumatom
vyak	bersifatkataketerangankasus
pxim	PerdanaMenteri
syum	strontiumatom
sy6n	kecurigaan
hy6n	menghidupkanjeniskelamin
pxac	empatikegembiraan
vlin	kendaraanjeniskelamin
qwin	mineraljeniskelamin
hr6s	penyerahan
tf6n	toner
xris	pemakaiantarmuka
hw6n	umpatan
xlen	lisankatabenda
pyec	lembarkerja
vlim	mayaingatan
bres	kataprosesor
hqip	awankomputasi
vlic	mayamesin
pfam	spammer
dyap	keduabahasa
txek	n-root
dxen	datapertambangan
byip	bisnisorang
hs6c	dirikepercayaan
txim	mengacu
hb6t	perubahandariperistiwa
hdem	domainnama
dxon	berbedaperistiwa
lwaf	flashmendorong
sl6t	crastinaltegang
xyik	floppycakram
fyif	refleksifsuara
bvin	alamsemestajeniskelamin
hm6p	motherboard
tfip	transitifkatakerja
jlen	lokalitasjeniskelamin
xrap	bergaris
tc6n	penelitianjeniskelamin
sl6n	artistikjeniskelamin
xrek	sabarpelatuk
xwan	abstrakjeniskelamin
dxif	konferensivideo
dyip	tidaklangsungpidato
zrek	exocentrickasus
hvuk	membangkitkan
xwat	laluperfektif
hr6m	renium
txaf	dipicu
hmef	adilkatakerja
tx6t	tekseditor
txes	kolektifkatabenda
bvat	menganiaya
bvuk	absolutifkasus
ry6t	radian
vlis	mayarealitas
qren	planetjeniskelamin
gvak	gramatikal-kasus
bxok	botak
mrum	komputerprogram
ps6t	operasisistem
hfom	fonem
tlem	Bacabacahanyaingatan
pwam	spyware
kfif	intibenefaktif
qwit	rasionalitas
nlaf	menginstal
nwok	asosiatifkasus
dzas	tdkmemuaskan
swuk	superlatifkasus
syos	meringkas
kr6n	listrikarus
dvin	matijeniskelamin
pxom	propositivesuasanahati
vris	timbal-baliksuara
rwos	erosol
dr6t	distraktor
ht6k	telekomunikasi
slaf	sosialjaringan
lyuk	berturut-turutayat
qras	atasan
ty6n	terminal
gwet	segeramasadepantegang
hp6s	aplikatifsuara
tsof	antipasifsuara
ht6p	continuativeaspek
hn6f	necessitativesuasanahati
pfip	pasifpartisip
glap	toolbar
sy6k	eksistensialayat
qyat	bercahayaintensitas
hy6t	gramatikal-objek
vyan	pengucapan
sluf	singulativejumlah
dvit	terlalubanyaklamanya
qyak	tunggaltindakankatakerja
bzik	fisika
xlet	superlatifgelar
hqap	menyajikanpartisip
qwat	jumlahdarizat
qyik	ikatpartikel
sr6t	superkritis-cairan
hfem	epentheticmorfem
tl6t	terpencilmasadepantegang
mrif	mikropon
pcuk	postpositionalkasus
tfim	acakmengaksesingatan
fwim	ethidium
ly6t	allocutionperjanjian
hvap	gulirbar
hfep	ygberulang-ulangaspek
bvan	distributifkataganti
fyun	floating-point-nomor
xyas	penghapusan
hvif	verba-akhiran
xyis	lalupasifpartisip
tl6n	pusatpengolahansatuan
tfok	lokatifdirectionalkasus
qwan	seringtanyapertanyaan
